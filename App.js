import React from 'react'
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Splash from './MiniProject/screen/SplashScreen';
import Welcome from './MiniProject/screen/WelcomeScreen';
import Login from './MiniProject/screen/LoginScreen';
import Register from './MiniProject/screen/RegisterScreen';
import Home from './MiniProject/screen/HomeScreen';
import Cart from './MiniProject/screen/CartScreen';
import Chat from './MiniProject/screen/ResepScreen';
import About from './MiniProject/screen/AboutScreen';
import Detail from './MiniProject/screen/Detail';

const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();
const RootStack = createStackNavigator();



const iconTab = ({ route }) => {
  return ({
      tabBarIcon: ({ focused, size }) => {
          if (route.name == 'Home') {
              return <FontAwesome5 name={'home'} size={22} color={'#139038'} solid/>
          } else if (route.name == 'Cart') {
              return <FontAwesome5 name="shopping-cart" size={22} color={'#139038'} solid />
          } else if (route.name == 'Resep') {
              return <FontAwesome5 name="comment" size={22} color={'#139038'} solid />
          } else if (route.name == 'About') {
              return <FontAwesome5 name="user" size={22} color={'#139038'} solid />
        }
      },
  });
}



const TabsStackScreen = () => (
  <TabsStack.Navigator screenOptions={iconTab} >

      <TabsStack.Screen name='Home' component={Home}
          options={{
              title: 'Home'
          }} />

      <TabsStack.Screen name='Cart' component={Cart}
          options={{
              title: 'Keranjang'
          }} />

      <TabsStack.Screen name='Resep' component={Chat}
          options={{
              title: 'Resep'
          }} />

      <TabsStack.Screen name='About' component={About}
          options={{
              title: 'Profil'
          }} />

  </TabsStack.Navigator>
);



const DrawerStackScreen = () => (
  <DrawerStack.Navigator >

      <DrawerStack.Screen name='TabsStackScreen' component={TabsStackScreen}/>

  </DrawerStack.Navigator>
);




const RootStackScreen = () => (
  <RootStack.Navigator>

      <RootStack.Screen name='Splash' component={Splash} options={{headerShown: false}}/>
      <RootStack.Screen name='Welcome' component={Welcome} options={{headerShown: false}}/>
      <RootStack.Screen name='Login' component={Login} options={{headerShown: false}}/>
      <RootStack.Screen name='Register' component={Register} options={{headerShown: false}}/>
      <RootStack.Screen name='DrawerStackScreen' component={DrawerStackScreen} options={{headerShown: false}}/>
      <RootStack.Screen name='Detail' component={Detail} options={{headerShown: false}}/>
      


  </RootStack.Navigator>
);


export default () => (
  <NavigationContainer>
      <RootStackScreen />
  </NavigationContainer>
);


const styles = StyleSheet.create({
  iconbawah: {
    color: '#139038'
  }
})


// import { StatusBar } from 'expo-status-bar';
// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// import { useFonts } from 'expo-font';
// import Detail from './MiniProject/screen/Detail'
// import ChatScreen from './MiniProject/screen/ChatScreen';

// export default function App() {
        
//   return (
//     <ResepScreen/>
//   );
// }

