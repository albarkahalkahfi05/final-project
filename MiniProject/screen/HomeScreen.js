import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TextInput, ScrollView, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

const HomeScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            
            <View style={{backgroundColor: '#FFFFFF', height: 60, width: 360, marginBottom: 10}}>

                <View style={{flexDirection: 'row', marginTop: 10}}>

                    <View style={{flexDirection: 'row'}}>
                        
                        <TextInput style={styles.inputsearch} placeholder = "Search" />
                        <Image source={require('../assets/logo/icon/search.png')} style={styles.iconsearch} />
                        
                    </View>

                    <View style={{flexDirection: 'row', marginLeft: 195}}>
                        <Image source={require('../assets/logo/icon/Love.png')} style={styles.iconpendukung} />
                        <Image source={require('../assets/logo/icon/Pesan.png')} style={styles.iconpendukung} />
                        <Image source={require('../assets/logo/icon/Notif.png')} style={styles.iconpendukung} />
                    </View>
                    
                </View>

            </View>
            
            <ScrollView>
                <Image source={require('../assets/logo/home1.jpg')} style={styles.fotohome} />

                <Text style={styles.headbuah}>Info Produk Tani</Text>

                <View style={{flexDirection: 'row'}}>

                    <TouchableOpacity  onPress={ () => navigation.navigate('Detail', {
                        id: 1,
                        namabuah: 'Telur',
                        keterangan: 'Telur adalah salah satu bahan makanan hewani yang dikonsumsi selain daging, ikan dan susu. Umumnya telur yang dikonsumsi berasal dari jenis-jenis burung, seperti ayam, bebek, dan angsa, akan tetapi telur-telur yang lebih kecil seperti telur ikan kadang juga digunakan sebagai campuran dalam hidangan.',
                    })}>
                        <View style={styles.list1}>
                            <Image source={require('../assets/logo/icon/telur1.jpg')} style={styles.fotoicon} />
                            <Text style={styles.teksicon}>Telur</Text>
                        </View>
                    </TouchableOpacity>

                    
                    <TouchableOpacity  onPress={ () => navigation.navigate('Detail', {
                        id: 2,
                        namabuah: 'Sayuran',
                        keterangan: 'Sayur atau sayuran merupakan sebutan umum bagi bahan pangan nabati yang biasanya mengandung kadar air yang tinggi, yang dapat dikonsumsi setelah dimasak atau diolah dengan teknik tertentu, atau dalam keadaan segar.[1][2] Istilah untuk kumpulan berbagai jenis sayur adalah sayur-sayuran atau sayur-mayur. ',
                    })}>
                        <View style={styles.list}>
                            <Image source={require('../assets/logo/icon/sayur1.jpg')} style={styles.fotoicon} />
                            <Text style={styles.teksicon}>Sayur</Text>
                        </View>
                    </TouchableOpacity>

                    
                    <TouchableOpacity  onPress={ () => navigation.navigate('Detail', {
                        id: 3,
                        namabuah: 'Daging',
                        keterangan: 'Daging ialah bagian lunak pada hewan yang terbungkus kulit dan melekat pada tulang yang menjadi bahan makanan. Daging tersusun sebagian besar dari jaringan otot, ditambah dengan lemak yang melekat padanya, urat, serta tulang rawan.',
                    })}>
                        <View style={styles.list}>
                            <Image source={require('../assets/logo/icon/daging1.jpg')} style={styles.fotoicon} />
                            <Text style={styles.teksicon}>Daging</Text>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity  onPress={ () => navigation.navigate('Detail', {
                        id: 4,
                        namabuah: 'Ikan',
                        keterangan: 'Lele atau ikan keli, adalah sejenis ikan yang hidup di air tawar. Lele mudah dikenali karena tubuhnya yang licin, agak pipih memanjang, serta memiliki "kumis" yang panjang, yang mencuat dari sekitar bagian mulutnya.',
                    })}>
                        <View style={styles.list}>
                            <Image source={require('../assets/logo/icon/ikan1.jpg')} style={styles.fotoicon} />
                            <Text style={styles.teksicon}>Ikan</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <Text style={styles.headfresh}>Aneka Kuliner</Text>
            
                {/* <Fruit/> */}

            </ScrollView>
        </View>
    )
}


export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5F5F5',
        alignItems: 'center',
        marginTop: 35,
    },
    iconsearch: {
        width: 35,
        height: 35,
        marginTop: 2,
        marginLeft: -225,
    },
    inputsearch: {
        width: 240,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#A6A6A6',
        paddingLeft: 10,
        marginBottom: 10,
        marginLeft: 10,
        backgroundColor: '#F3F3F3',
        paddingLeft: 50
    },
    iconpendukung: {
        width: 35,
        height: 35,
        marginTop: 2,
    },
    fotohome: {
        width: 332,
        height: 190,
        borderRadius: 5,
        marginLeft: 13
    },
    list1: {
        backgroundColor: '#FFFFFF',
        height: 110,
        width: 75,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#EAEAEA',
        marginRight: 14,
        marginLeft: 13,
    },
    list: {
        backgroundColor: '#FFFFFF',
        height: 110,
        width: 75,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#EAEAEA',
        marginRight: 14
    },
    fotoicon: {
        marginTop: 5,
        marginLeft: 7,
        width: 60,
        height: 60,
        marginBottom: 10
    },
    teksicon: {
        color: '#000000',
        fontSize: 12,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    headfresh: {
        marginLeft: 12,
        fontWeight: 'bold',
        color: '#139038',
        fontSize: 20,
        marginTop: 15,
        marginBottom: 5
    },
    headbuah: {
        marginLeft: 12,
        fontWeight: 'bold',
        color: '#139038',
        fontSize: 20,
        marginTop: 15,
    },
    datafruit: {
        width: 120,
        height: 150,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        marginRight: 10,
        marginLeft: 12,
        flexDirection: 'row',
        marginBottom: 10
        
    },
    headvege: {
        marginLeft: 12,
        fontWeight: 'bold',
        color: '#139038',
        fontSize: 20,
        marginTop: 15,
        marginBottom: 5
    },
    datavege: {
        width: 120,
        height: 150,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        marginRight: 10,
        marginLeft: 12
        
    },
})
