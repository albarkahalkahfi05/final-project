import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'


function DetailScreen({ route, navigation }) {
console.log(route)
const { id } = route.params;
const { namabuah } = route.params;
const { keterangan } = route.params;

    if(id == 1){

        return (
            <View style={styles.container}>

                
                <Image source={require('../assets/buah/telur.jpg')} style={styles.fotobuah} />

                <View style={styles.kotakdetail}>
                    <View style={styles.namabuah}>
                        <Text style={styles.teksnamabuah}>Nama Produk : <Text style={styles.teksnamabuah}>{JSON.stringify(namabuah)}</Text></Text>  
                    </View>

                    <View style={styles.keteranganbuah}>
                        <Text style={styles.teksketeranganbuah}>Keterangan : <Text style={styles.teksketeranganbuah}>{JSON.stringify(keterangan)}</Text></Text>
                    </View>
                </View>
            </View>
        )

    } else if (id == 2) {
        
        return (
            <View style={styles.container}>

                
                <Image source={require('../assets/buah/sayur.jpg')} style={styles.fotobuah} />

                <View style={styles.kotakdetail}>
                    <View style={styles.namabuah}>
                        <Text style={styles.teksnamabuah}>Nama Produk : <Text style={styles.teksnamabuah}>{JSON.stringify(namabuah)}</Text></Text> 
                    </View>

                    <View style={styles.keteranganbuah}>
                        <Text style={styles.teksketeranganbuah}>Keterangan : <Text style={styles.teksketeranganbuah}>{JSON.stringify(keterangan)}</Text></Text>
                    </View>
                </View>
            </View>
        )

    } else if (id == 3) {
        
        return (
            <View style={styles.container}>

                
                <Image source={require('../assets/buah/daging.jpg')} style={styles.fotobuah} />

                <View style={styles.kotakdetail}>
                    <View style={styles.namabuah}>
                        <Text style={styles.teksnamabuah}>Nama Produk : <Text style={styles.teksnamabuah}>{JSON.stringify(namabuah)}</Text></Text> 
                    </View>

                    <View style={styles.keteranganbuah}>
                        <Text style={styles.teksketeranganbuah}>Keterangan : <Text style={styles.teksketeranganbuah}>{JSON.stringify(keterangan)}</Text></Text> 
                    </View>
                </View>
            </View>
        )

    } else if (id == 4) {
        
        return (
            <View style={styles.container}>

                
                <Image source={require('../assets/buah/lele.jpg')} style={styles.fotobuah} />

                <View style={styles.kotakdetail}>
                    <View style={styles.namabuah}>
                        <Text style={styles.teksnamabuah}>Nama Produk : <Text style={styles.teksnamabuah}>{JSON.stringify(namabuah)}</Text></Text>    
                    </View>

                    <View style={styles.keteranganbuah}>
                        <Text style={styles.teksketeranganbuah}>Keterangan : <Text style={styles.teksketeranganbuah}>{JSON.stringify(keterangan)}</Text></Text>   
                    </View>
                </View>
            </View>
        )

    }

}

export default DetailScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        marginTop: 40
    },
    fotobuah: {
        width: 330,
        height: 330,
        borderColor: '#000000',
        marginLeft: 15
    },
    namabuah: {
        marginLeft: 0,
        flexDirection: 'row',
        backgroundColor: '#000000',
        height: 35,
        borderRadius: 5,
        marginBottom: 5,
        width: 360
    },
    teksnamabuah: {
        color: '#FFFFFF',
        fontSize: 18,
        marginLeft: 15,
        marginTop: 3,
    },  
    keteranganbuah: {
        marginLeft: 0,
        flexDirection: 'row',
        backgroundColor: '#139038',
        borderRadius: 5,
        height: 500,
        width: 360
    },
    teksketeranganbuah: {
        fontSize: 14,
        marginLeft: 15,
        marginTop: 10,
        color: '#FFFFFF'
    },
    kotakdetail: {
        borderColor: '#000000',
        width: 330
    }
})
