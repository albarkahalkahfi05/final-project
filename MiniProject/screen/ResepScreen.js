import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { FlatList, StyleSheet, Text, View, TextInput, Button, Image } from 'react-native'

const ResepScreen = () => {
    const [recipes, setRecipes] = useState([])  

    const getRecipes = async () =>{
        let res = await axios.get('https://masak-apa-tomorisakura.vercel.app/api/recipes')
        setRecipes(res.data.results)
    }
    // console.log(recipes)

    useEffect(()=>{
        getRecipes()
    },[]) 
    return (
        <View style={styles.container}>
             
            <FlatList 
                data={recipes}
                keyExtractor= {(item, index) => `${item.key} - ${index}`}
                renderItem={({item})=>{
                    return(
                        <View  style={{marginBottom:20}}>
                            <Text>{item.title}</Text>
                            <Text>{item.times}</Text>
                            <Text>{item.portion}</Text>
                            <Text>{item.dificulty}</Text>
                            <Image style={styles.image} source={{uri: item.thumb}} resizeMode="cover" />
                        </View>
                    )
                }}
            />
        </View>
    )
}

export default ResepScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin:50
    },
    image: {
        width:"100%",
        height:180,
        borderRadius:10,
        marginBottom:5
    },
    title:{
        textAlign:'center',
        fontWeight: "bold",
        fontSize:18,
        color:"#333333"
    },
    underLine:{
        width:"100%",
        backgroundColor:"#61DEA4",
        height:3
    }

})
