import axios from 'axios'
import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image, FlatList } from 'react-native'

const RegisterScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>

            <Text style={styles.tekssignup}>REGISTRASI</Text>
            <Text style={styles.tekssign}>Isi detailnya dan isi akun anda</Text>

            <View style={styles.formusername}>
                <Text style={styles.teksform}>Nama Lengkap</Text>
                <TextInput style={styles.input}/>
            </View>

            <View>
                <Text style={styles.teksform}>Username</Text>
                <TextInput style={styles.input}/>
            </View>

            <View>
                <Text style={styles.teksform}>Nomor Telepon</Text>
                <TextInput style={styles.input}/>
            </View>

            <View>
                <Text style={styles.teksform}>Email</Text>
                <TextInput style={styles.input}/>
            </View>

            <View>
                <Text style={styles.teksform}>Password</Text>
                <TextInput style={styles.input}/>
            </View>

            <View>
                <Text style={styles.teksform}>Ulangi Password</Text>
                <TextInput style={styles.input}/>
            </View>

            <View style={{marginTop: 10, marginLeft: 30}}>
                <TouchableOpacity style={styles.btnlog} onPress={ () => navigation.navigate('DrawerStackScreen')} >
                    <Text style={styles.btnlogteks}>Daftar</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.menuvia}>
                <Text style={styles.via}>atau login via sosial media</Text>

                <View style={styles.logovi}>
                    <Image source={require('../assets/logo/fb.png')} style={styles.logofb} />
                    <Image source={require('../assets/logo/tweet.png')} style={styles.logotweet} />
                    <Image source={require('../assets/logo/ig.png')} style={styles.logoig} />
                    <Image source={require('../assets/logo/google.png')} style={styles.logogoogle} />
                </View>

                <View style={{flexDirection: 'row', marginTop: 10}}>
                    <Text style={styles.via}>Sudah memiliki akun?</Text><Text style={styles.via1} onPress={ () => navigation.navigate('Login')} > Masuk</Text>
                </View>
            </View>

        </View>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginTop: 40,
    },
    tekssignup: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#139038',
    },
    tekssign: {
        fontSize: 12,
        color: '#656565',
        marginTop: 10
    },
    formusername: {
        marginTop: 20,
    },
    teksform: {
        color: '#139038',
        fontWeight: 'bold',
        marginBottom: 5
    },
    input: {
        width: 300,
        height: 40,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#A6A6A6',
        marginBottom: 10,
        paddingLeft: 10
    },
    btnlog: {
        padding: 10,
        backgroundColor: '#24903B',
        borderRadius: 20,
        width: 130,
        height: 40,
        marginRight: 30
    },
    btnlogteks: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    menuvia: {
        marginTop: 20
    },
    via: {
        textAlign: "center",
        fontSize: 14,
        color: '#656565'
    },
    via1: {
        textAlign: "center",
        fontSize: 14,
        color: '#139038',

    },
    logovi: {
        flexDirection: 'row',
        marginTop: 5,
        marginLeft: 20
    },
    logofb: {
        width: 40,
        height: 38,
        alignItems: 'center',
        marginLeft: -20
    },
    logogoogle: {
        width: 43,
        height: 43,
        alignItems: 'center',
        marginLeft: -6
    },
    logotweet: {
        width: 50,
        height: 50,
        alignItems: 'center',
        marginTop: -5
    },
    logoig: {
        width: 57,
        height: 57,
        alignItems: 'center',
        marginTop: -6,
        marginLeft: -6
    }
})
