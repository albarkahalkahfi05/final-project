import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Router from './router'
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
//import {store} from './redux';

export default function index() {
    return (
        // <Provider store={store}>
            <NavigationContainer>
                <Router />
            </NavigationContainer>
        // </Provider>
    )
}

const styles = StyleSheet.create({})
